#ifndef TEST_UTILS_HPP
#define TEST_UTILS_HPP

namespace test_utils
{

// Initialize a full game session
void init_all();

void cleanup_all();

} // test_utils

#endif // TEST_UTILS_HPP
