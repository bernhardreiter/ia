#include "paths.hpp"

namespace paths
{

const std::string gfx_path = "res/gfx";

const std::string fonts_path = gfx_path + "/fonts";
const std::string tiles_path = gfx_path + "/tiles/24x24";
const std::string images_path = gfx_path + "/images";

const std::string logo_img_path = images_path + "/main_menu_logo.png";
const std::string skull_img_path = images_path + "/skull.png";

} // paths
